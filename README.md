Sublime Text 2&3 - RAISE Specification Language
=============================================

A Sublime Text package for RAISE Specification Language - [RSL][0]

## Prerequisites
You should have following software installed and running

 * [rsltc][3]

## Installation

 * Install [Package Manager][2].
 * Use `Cmd+Shift+P` or `Ctrl+Shift+P` then `Package Control: Install Package`.
 * Look for `RSL` and install it.

If you prefer to install manually, install git, then:

    git clone https://bitbucket.org/vuhonglinh/sublimetext-rsl "<Sublime Text 2 Packages folder>/RSL"
## Credits

This code is available on [Bitbucket][1]. Pull requests are welcome.

Created by Linh Vu Hong.

 [0]: http://en.wikipedia.org/wiki/RAISE 
 [1]: https://bitbucket.org/vuhonglinh/sublimetext-rsl
 [2]: http://wbond.net/sublime_packages/package_control
 [3]: http://www.iist.unu.edu/newrh/III/3/1/docs/rsltc/
